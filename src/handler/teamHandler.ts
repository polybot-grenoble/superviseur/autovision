import { readFileSync, writeFileSync } from "fs";
import { Logger, LogLevel, LogOrigin } from "../logger.js";
import { command, CommandOption, Response } from "../parser.js";
import { AutoVision } from "../autovision.js";

export class TeamHandler {
    logger: Logger;
    team: team;

    constructor(logger: Logger, team: team){
        this.logger = logger;
        this.team = team;

        for(let i=0;i<4;i++){
            if(this.team.default_tags[i] != 0) logger.log("Robot " + i + " has default tag : " + this.team.default_tags[i], LogLevel.INFO, LogOrigin.TEAM);
        }

        if(this.team.default_side == TeamType.BLEUE || this.team.default_side == TeamType.VERTE) logger.log("Default side : " + team.default_side, LogLevel.INFO, LogOrigin.TEAM);
    }

    handleCommand(cmd: command, cb: any){
        let res: number;
        let id: number;
        
        switch(cmd.option){
            case CommandOption.TAG_GET:
                if(cmd.argument.length != 1) return cb(new Response(Response.BAD_COMMAND));

                res = parseInt(cmd.argument[1]);
                if(res<0 || res>3) return cb(new Response(Response.BAD_COMMAND));

                cb(new Response(Response.OK, [this.getTag(res)+""]));
                break;

            case CommandOption.TAG_SET:
                if(cmd.argument.length != 2) return cb(new Response(Response.BAD_COMMAND));
                
                res = parseInt(cmd.argument[0]);
                if(res<0 || res>3) return cb(new Response(Response.BAD_COMMAND));

                id = parseInt(cmd.argument[1]);
                if(id < 0 || id > 9) return cb(new Response(Response.BAD_COMMAND));
                
                this.setTag(res,id);
                cb(new Response(Response.OK));
                break;

            case CommandOption.TEAM_GET:
                cb(new Response(Response.OK, [this.getSide()+""]));
                break;

            case CommandOption.TEAM_SET:
                if(cmd.argument.length != 1) return cb(new Response(Response.BAD_COMMAND));

                res = parseInt(cmd.argument[0]);
                if(res != TeamType.BLEUE && res != TeamType.VERTE) return cb(new Response(Response.BAD_COMMAND));

                this.setSide(res);
                cb(new Response(Response.OK));
                break;

            case CommandOption.TEAM_SAVE:
                res = this.writeSettings();
                if(res == 1) return cb(new Response(Response.ERROR, ["Team config not complete."]));
                if(res == 2) return cb(new Response(Response.BUSY));
                if(res == 3) return cb(new Response(Response.ERROR, ["Failed to edit team config."]));
                cb(new Response(Response.OK));
                break;

            case CommandOption.TEAM_BATCH:
                if(cmd.argument.length!=5) return cb(new Response(Response.BAD_COMMAND));

                /* Team set */
                res = parseInt(cmd.argument[0]);
                if(res != TeamType.BLEUE && res != TeamType.VERTE) return cb(new Response(Response.BAD_COMMAND));
                this.setSide(res);
                
                /* Tag set */
                for(let i = 0 ; i < 4 ; i++){
                    id = parseInt(cmd.argument[i+1]);
                    if(id < 0 || id > 9) return cb(new Response(Response.BAD_COMMAND));
                    this.setTag(i,id);
                }

                /* Write team */
                res = this.writeSettings();
                if(res == 1) return cb(new Response(Response.ERROR, ["Team config not complete."]));
                if(res == 2) return cb(new Response(Response.BUSY));
                if(res == 3) return cb(new Response(Response.ERROR, ["Failed to write team config."]));
                cb(new Response(Response.OK,["Team config saved."]));
                break;
                
            default:
                cb(new Response(Response.BAD_COMMAND));
                break;
        }
    }

    setTag(robot: number, tag: number): void {
        this.team.default_tags[robot] = tag;
        this.logger.log("Robot " + robot + " tag set to : " + tag, LogLevel.INFO, LogOrigin.TEAM);
    }

    getTag(robot: number): number {
        if(!this.team.default_tags[robot]) return -1;
        return this.team.default_tags[robot];
    }

    setSide(team: number): void {
        this.team.default_side = team;
        this.logger.log("Team set to : " + team, LogLevel.INFO, LogOrigin.TEAM);
    }

    getSide(): TeamType {
        return this.team.default_side;
    }

    writeSettings(): number{
        if(AutoVision.config_busy || AutoVision.team_busy) return 2;
        AutoVision.config_busy = true;

        let vConfig: any;

        try {
            vConfig = JSON.parse(readFileSync(process.env.VISION_CONFIG_FOLDER+process.env.VISION_CONFIG_FILE, "utf-8"));
        }catch(err){
            this.logger.log("Failed to open team config.", LogLevel.ERROR, LogOrigin.TEAM);
            AutoVision.config_busy = false;
            return 3;
        }

        for(let i = 0; i<2; i++){
            vConfig["robot"+(i+1)+"_id"]=this.team.default_tags[i];
        }

        for(let i = 0; i<2 ; i++){
            vConfig["ennemi"+(i+1)+"_id"]=this.team.default_tags[i+2];
        }

        if(this.team.default_side != TeamType.BLEUE && this.team.default_side != TeamType.VERTE){
            this.logger.log("Side is not set.", LogLevel.ERROR);
            AutoVision.config_busy = false;
            return 1;
        }

        vConfig["equipe"]=this.team.default_side;
        vConfig["pos_camera"]=this.team.sides[this.team.default_side].camera_pos;

        try{
            writeFileSync(process.env.VISION_CONFIG_FOLDER+process.env.VISION_CONFIG_FILE, JSON.stringify(vConfig, undefined, 4), "utf-8");
        }catch(err){
            this.logger.log("Failed to write team config.", LogLevel.ERROR, LogOrigin.TEAM);
            AutoVision.config_busy = false;
            return 3;
        }

        this.logger.log("Saved team config.", LogLevel.INFO, LogOrigin.TEAM);

        AutoVision.config_busy = false;
        AutoVision.team_busy = true;
        return 0;
    }
}

export type team = {
    default_tags: number[];
    default_side: TeamType;
    sides: teamSide[];
}

export type teamSide = {
    name: TeamType;
    camera_pos: number[];
}

export const enum TeamType {BLEUE=0, VERTE=1};
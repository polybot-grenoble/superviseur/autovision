import { command, CommandOption, Response } from "../parser.js";
import { writeFileSync, readFileSync } from "fs";
import { exit } from "process";
import { Logger, LogLevel, LogOrigin } from "../logger.js";
import { execFileSync } from "child_process";
import { AutoVision } from "../autovision.js";

export class CameraHandler {
    private cameras: camera[];
    camera: camera;
    
    private logger: Logger;

    constructor(logger: Logger, cameras: camera[], force_camera?: string){
        this.logger = logger;
        this.cameras = cameras;

        if(force_camera){
            this.camera = this.cameras.filter(c => (c.name == force_camera))[0];
        }else{
            this.camera = this.getInstalledCameraDriver();
        }

        if(!this.camera){
            logger.log("Unknown installed camera. Configure it in the config file first.", LogLevel.CRITICAL);
            exit(1);
        }

        logger.log("Using camera : " + this.camera.name, LogLevel.INFO);

        this.loadCalibrationData();
    }

    changeCamera(name: string): Response {
        if(AutoVision.camera_busy) return new Response(Response.BUSY);
        AutoVision.camera_busy = true;

        if(!name) return new Response(Response.BAD_COMMAND);

        let new_camera = this.cameras.filter(c => (c.name == name))[0];
        if(!new_camera){
            this.logger.log("Unknown installed camera. Configure it in the config file first.", LogLevel.ERROR);
            AutoVision.camera_busy = false;
            return new Response(Response.ERROR, ["Unknown camera."]);
        }

        try {
            let data = readFileSync('/boot/config.txt', 'utf-8');
            data=data.replace("dtoverlay="+this.camera.driver,"dtoverlay="+new_camera.driver);
            writeFileSync('/boot/config.txt',data,'utf-8');
        }catch (err){
            console.log(err)
            this.logger.log("Failed to change boot file.", LogLevel.ERROR);
            AutoVision.camera_busy = false;
            return new Response(Response.ERROR, ["Failed to modify boot file."]);
        }

        this.logger.log("Shutdown the computer to apply changes.", LogLevel.INFO);
        AutoVision.camera_busy = false;

        return new Response(Response.OK, ["Reboot the computer to apply changes."]);
    }

    takePhoto(overlay: boolean): string {
        if(AutoVision.camera_busy) return null;
        AutoVision.camera_busy = true;
        
        let res: string;
        let dest: string = process.env.VISION_IMAGE_FOLDER + (overlay ? "overlay.png" : "raw.png");
        let exe: string;
        let args: string[];

        if(overlay){
            exe = process.env.VISION_EXE_FOLDER + process.env.PHOTO_EXE;
            args = [process.env.VISION_CONFIG_FOLDER+process.env.VISION_CONFIG_FILE,
                "1", process.env.VISION_IMAGE_FOLDER+"raw.png", process.env.VISION_IMAGE_FOLDER+"overlay.png", "1"];
        }else{
            exe = process.env.VISION_EXE_FOLDER + process.env.BRUT_EXE;
            args = [process.env.VISION_CONFIG_FOLDER+process.env.VISION_CONFIG_FILE, dest, "1"];
        }

        try{
            execFileSync(exe, args);
        }catch(err){
            this.logger.log("Failed to take picture : " + err.status, LogLevel.ERROR);
            AutoVision.camera_busy = false;
            return null;
        }
        
        try{
            res = readFileSync(dest).toString("base64");
        }catch(err){
            this.logger.log("Failed to encode picture : " + err.message, LogLevel.ERROR);
        }

        AutoVision.camera_busy = false;
        return res;
    }

    listCameras(): string[] {
        let res: string[] = [];
        for(let i=0;i<this.cameras.length;i++){
            res.push(this.cameras[i].name, this.cameras[i].driver, this.cameras[i].resolution[0]+"", this.cameras[i].resolution[1]+"", this.cameras[i].type, this.cameras[i].default + "");
        }
        return res;
    }

    getCamera(): string[] {
        return [this.camera.name, this.camera.driver, this.camera.resolution[0]+"", this.camera.resolution[1]+"", this.camera.type, this.camera.default + ""];
    }

    handleCommand(cmd: command, cb: any): void {
        let photo: string;
        switch(cmd.option){
            case CommandOption.CAMERA_GET:
                cb(new Response(Response.OK, this.getCamera()));
                break;
            case CommandOption.CAMERA_LIST:
                cb(new Response(Response.OK, this.listCameras()));
                break;
            case CommandOption.CAMERA_SET:
                cb(this.changeCamera(cmd.argument[0]));
                break;
            case CommandOption.CAMERA_RAW:
                photo = this.takePhoto(false);
                if(!photo) return cb(new Response(Response.ERROR, ["Failed to take photo."]));
                cb(new Response(Response.OK, [photo]));
                break;
            case CommandOption.CAMERA_OVERLAY:
                photo = this.takePhoto(true);
                if(!photo) return cb(new Response(Response.ERROR, ["Failed to take photo."]));
                cb(new Response(Response.OK, [photo]));
                break;
            default:
                cb(new Response(Response.BAD_COMMAND));
                break;
        }
    }

    private loadCalibrationData(): void {
        if(AutoVision.config_busy){
            this.logger.log("Config file is busy.", LogLevel.ERROR, LogOrigin.CONFIG);
            return;
        }
        AutoVision.config_busy = true;

        let config: any;
        let camCalibration: any;

        try {
            config = JSON.parse(readFileSync(process.env.VISION_CONFIG_FOLDER+process.env.VISION_CONFIG_FILE, "utf-8"));         
        }catch(err){
            this.logger.log("Failed to open vision config file. Code: " + err.code, LogLevel.ERROR);
            this.logger.log("Calibration data might be wrong.", LogLevel.WARNING);
            AutoVision.config_busy = false;
            return;
        }

        try {
            camCalibration = JSON.parse(readFileSync(process.env.VISION_CONFIG_FOLDER+this.camera.name+"/calib_"+this.camera.name+".json", "utf-8"));         
        }catch(err){
            this.logger.log("Cannot open camera " + this.camera.name + " calibration file: " + err.code, LogLevel.ERROR);
            this.logger.log("Calibration data might be wrong.", LogLevel.WARNING);
            AutoVision.config_busy = false;
            return;
        }

        if(!config || !camCalibration){
            this.logger.log("Vision related files are empty.", LogLevel.CRITICAL);
            exit(1);
        }

        config["focus"]=camCalibration["focus"];
        config["zoom"]=camCalibration["zoom"];
        config["resolution"]=camCalibration["resolution"];
        config["camera"]=camCalibration["camera"];
        config["dist_coeffs"]=camCalibration["dist_coeffs"];
        if(this.camera.type == "fisheye" || this.camera.type == "zf_fisheye"){
            config["fisheye"]=1;
        }else{
            config["fisheye"]=0;
        }

        try {
            writeFileSync(process.env.VISION_CONFIG_FOLDER+process.env.VISION_CONFIG_FILE,JSON.stringify(camCalibration, undefined, 4), "utf-8");
        }catch(err){
            this.logger.log("Failed to write vision config file: " + err.code, LogLevel.ERROR);
            this.logger.log("Calibration data might be wrong.", LogLevel.WARNING);
            AutoVision.config_busy = false;
            return;
        }

        AutoVision.config_busy = false;
    }

    private getInstalledCameraDriver(): camera {
        try{
            let out = execFileSync("libcamera-hello", ["--list-cameras"], {timeout: 5000}).toString();
            let reg = out.match(/0 : (?<cam>\w+)/g)[0].split(" : ")[1];
    
            if(reg.length == 0){
                this.logger.log("No driver found", LogLevel.CRITICAL);
                exit(1);
            }
            
            return this.cameras.filter(c => (c.driver==reg && c.default))[0];
        }catch(err){
            this.logger.log("Failed to fetch camera driver.", LogLevel.CRITICAL);
            exit(1);
        } 
    }
}

type type_camera = 'normal' | 'zf' | 'fisheye' | 'zf_fisheye'; 

export type camera = {
    name: string;
    driver: string;
    type: type_camera;
    resolution: number[];
    default?: true;
}
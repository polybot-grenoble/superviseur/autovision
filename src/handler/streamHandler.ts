import {ChildProcessWithoutNullStreams, execFileSync, spawn, spawnSync} from 'node:child_process';
import { createWriteStream, existsSync, mkdirSync, WriteStream } from 'node:fs';
import { Logger, LogLevel, LogOrigin } from '../logger.js';
import { command, Response, CommandOption } from '../parser.js';
import { GameState, parseGameState, rawDataToCSV } from '../ipc.js';
import { AutoVision } from '../autovision.js';

export class StreamHandler {
    private vision_proc: ChildProcessWithoutNullStreams;

    private start_time: number;

    private fps: number;

    private csvStream: WriteStream;

    private logger: Logger;

    private cache: GameState;

    constructor(logger: Logger){
        this.logger = logger;

        if(!existsSync("data")) mkdirSync("data");
    }

    private setup(): boolean {
        try {
            let exe = process.env.VISION_EXE_FOLDER + process.env.PHOTO_EXE;
            let args = [process.env.VISION_CONFIG_FOLDER+process.env.VISION_CONFIG_FILE,
                "1", process.env.VISION_IMAGE_FOLDER+"raw.png", process.env.VISION_IMAGE_FOLDER+"overlay.png", "1"];
            execFileSync(exe, args)
        }catch(error){
            this.logger.log("Error during setup. " + error, LogLevel.ERROR, LogOrigin.STREAM);
            return false;
        }
        return true;
    }

    private start(): number {
        if(AutoVision.stream_busy || AutoVision.camera_busy || AutoVision.config_busy) return 1;
        AutoVision.stream_busy = true;
        AutoVision.camera_busy = true;
        AutoVision.config_busy = true;

        if(!this.setup()){
            this.logger.log("Failed to complete setup. Stream scrubbed.", LogLevel.CRITICAL, LogOrigin.STREAM);
            AutoVision.stream_busy = false;
            AutoVision.camera_busy = false;
            AutoVision.config_busy = false;
            AutoVision.team_busy   = false;
            return 2;
        }

        if (!existsSync("data")) mkdirSync("data");
        this.csvStream = createWriteStream(fileNameFormat(),{encoding: 'utf-8', flags: 'a'});

        this.start_time = Date.now();
        
        this.vision_proc = spawn(process.env.VISION_EXE_FOLDER+process.env.VISION_EXE, [process.env.VISION_CONFIG_FOLDER+process.env.VISION_CONFIG_FILE]);

        this.vision_proc.stdout.on('data', (data: Buffer) => {
            this.cache = parseGameState(data.toString());
            this.csvStream.write(rawDataToCSV(data.toString()),'utf-8');
        });

        this.vision_proc.stderr.on("data", (data: Buffer) => {
            this.fps = parseInt(data.toString().split("/")[1]);
        });

        this.vision_proc.on('close', (code) => {
            AutoVision.stream_busy  = false;
            AutoVision.camera_busy  = false;
            AutoVision.team_busy    = false;
            AutoVision.config_busy  = false;
            this.logger.log("Stream stopped. Code: " + code, LogLevel.INFO, LogOrigin.STREAM);
        });

        this.vision_proc.on('error', (err)=>{
            this.logger.log("Stream error " + err.name  + " : " + err.message, LogLevel.CRITICAL, LogOrigin.STREAM);
        });

        this.logger.log("Stream started.", LogLevel.INFO, LogOrigin.STREAM);

        return 0;
    }

    private stop(): boolean {
        if(!AutoVision.stream_busy) return false;
        this.vision_proc.kill();
        this.csvStream.close();
        return true;
    }

    private sync(): string {
        return Date.now() + "";
    }

    private uptime(): number {
        if(!AutoVision.stream_busy) return -1;
        return (Date.now() - this.start_time)/1000;
    }

    private getFps(): number {
        if(!AutoVision.stream_busy) return 0;
        return this.fps;
    }

    private getStats(): string[] {
        return [AutoVision.stream_busy+"", this.uptime()+"", this.getFps()+""];
    }

    handleCommand(cmd: command, cb: any){
        let res: number;
        switch(cmd.option){
            case CommandOption.STREAM_START:
                res = this.start();
                if(res == 1) return cb(new Response(Response.ERROR, ["Stream already started."]));
                if(res == 2) return cb(new Response(Response.ERROR, ["Failed to complete setup. Stream scrubbed."]))
                cb(new Response(Response.OK, ["Stream started."]));
                break;

            case CommandOption.STREAM_SYNC:
                cb(new Response(Response.OK, [this.sync()]));
                break;
                
            case CommandOption.STREAM_KILL:
                if(!this.stop()) return cb(new Response(Response.ERROR, ["Stream not started."]));
                cb(new Response(Response.OK, ["Stream stopped."]));
                break;

            case CommandOption.STREAM_PERF:
                cb(new Response(Response.OK, this.getStats()));
                break;
                
            case CommandOption.STREAM_GET:
                if(!AutoVision.stream_busy) return cb(new Response(Response.ERROR, ["Stream is not started."]));
                if(!this.cache) return cb(new Response(Response.ERROR, ["No data available."]));
                cb(new Response(Response.OK, [JSON.stringify(this.cache)]));
                break;

            default:
                cb(new Response(Response.BAD_COMMAND));
                break;
        }
    }
}

function fileNameFormat(): string {
    let d = new Date(Date.now()).toISOString();
    d=d.replace(/[-:T]/g, "_").replace(/[Z.]/g,"");
    return "data/data_"+d+".csv";
}
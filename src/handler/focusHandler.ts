import { command, CommandOption, Response } from '../parser.js'
import { execFile, spawn } from 'node:child_process';
import { Logger, LogLevel } from '../logger.js';
import { AutoVision } from '../autovision.js';

export class FocusHandler{
    private focus_proc: any;
    private enable: boolean;

    private logger: Logger;

    constructor(logger: Logger, camera_type: string){
        this.enable = camera_type=="zf" || camera_type=="zf_fisheye";
        this.logger = logger;
        if(!this.enable) logger.log("czf commands disabled.", LogLevel.WARNING);
    }

    handleCommand(cmd: command, cb: any){
        if(!this.enable) return cb(new Response(Response.ERROR, ["Focus commands disabled."]));
        switch(cmd.option){
            case CommandOption.FOCUS_AUTOFOCUS:
                if(AutoVision.camera_busy) return cb(new Response(Response.BUSY));
                AutoVision.camera_busy = true;
                cb(new Response(Response.OK, ["Working."]));
                
                this.focus_proc = spawn("python3", [process.env.CZF_EXE_FOLDER+process.env.CZF_EXE, "-a"]);
                
                this.focus_proc.stderr.on('data', (data)=>{
                    this.logger.log("Autofocus failed: " + data, LogLevel.ERROR);
                });

                this.focus_proc.on('close', (code) => {
                    this.logger.log("Autofocus done.")
                    AutoVision.camera_busy = false;
                });
                break;
            
            case CommandOption.FOCUS_GET:
                if(AutoVision.camera_busy) return cb(new Response(Response.BUSY));
                AutoVision.camera_busy = true;

                execFile("python3", [process.env.CZF_EXE_FOLDER+process.env.CZF_EXE, "-f"], (error, stdout, stderr) => {
                    AutoVision.camera_busy = false;
                    if (error) {
                        cb(new Response(Response.ERROR, ["Could not get focus value. " + error]));
                        throw error;
                    }
                     cb(new Response(Response.OK, [stdout.trim()]));
                });
                break;
            
            case CommandOption.FOCUS_RESET:
                if(AutoVision.camera_busy) return cb(new Response(Response.BUSY));
                AutoVision.camera_busy = true;
                cb(new Response(Response.OK, ["Working."]));
                
                this.focus_proc = spawn("python3", [process.env.CZF_EXE_FOLDER+process.env.CZF_EXE, "-r"]);
                
                this.focus_proc.stderr.on('data', (data)=>{
                    this.logger.log("Focus reset failed: " + data, LogLevel.ERROR);
                });

                this.focus_proc.on('close', (code) => {
                    this.logger.log("Focus reset done.");
                    AutoVision.camera_busy = false;
                });
                break;
            
            case CommandOption.FOCUS_SET:
                if(AutoVision.camera_busy) return cb(new Response(Response.BUSY));
                if(cmd.argument.length != 1) return cb(new Response(Response.BAD_COMMAND))
                AutoVision.camera_busy = true;
                cb(new Response(Response.OK, ["Working."]));
                
                this.focus_proc = spawn("python3", [process.env.CZF_EXE_FOLDER+process.env.CZF_EXE, "-f", cmd.argument[0]]);
                
                this.focus_proc.stderr.on('data', (data)=>{
                    this.logger.log("Focus set failed: " + data, LogLevel.ERROR);
                });

                this.focus_proc.on('close', (code) => {
                    this.logger.log("Applied focus : " + cmd.argument[0]);
                    AutoVision.camera_busy = false;
                });
                break;
            
            default:
                cb(new Response(Response.BAD_COMMAND));
                break;
        }
    }
}
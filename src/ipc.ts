type sliceType = 'marron' | 'jaune' | 'magenta' | 'invalid';
const __slices: sliceType[] = [ "invalid", "marron", "jaune", "magenta" ];

type robotType = {
    type: 'robot',
    x: number,
    y: number,
    alpha: number,
    robot_id: number
};

type gatoType = {
    type: 'gato',
    x: number,
    y: number,
    pile_id: number,
    pile_height: number,
    slices: sliceType[]
};

type ceriseType = {
    type: 'cerise',
    cherry_zone: number, 
    cherry_count: number
}

type invalidType = {
    type: 'invalid'
}

export type IPCData = robotType | gatoType | ceriseType | invalidType;

export type GameState = {
    time: number,
    state: IPCData[]
}

function rawGameStateData (line: string): IPCData {
	let [ 
		type, x, y, alpha, 
		robot_id,
		pile_id, pile_height,
		slice_0, slice_1, slice_2,
		cherry_zone, cherry_count
	] = line.split(';');

    switch (type) {
        case "0":
            return {
                type: "robot",
                x: parseFloat(x),
                y: parseFloat(y),
                alpha: parseFloat(alpha),
                robot_id: parseInt(robot_id)
            }

        case "1":
            return {
                type: "gato",
                x: parseFloat(x),
                y: parseFloat(y),
                pile_id: parseInt(pile_id),
                pile_height: parseInt(pile_height),
                slices: [
                    __slices[parseInt(slice_0)],
                    __slices[parseInt(slice_1)],
                    __slices[parseInt(slice_2)]
                ]
            }
        
        case "2":
            return {
                type: "cerise",
                cherry_count: parseInt(cherry_count),
                cherry_zone: parseInt(cherry_zone)
            }

        default:
            return { type: "invalid" }
    }

}

export function parseGameState (raw: string): GameState {
    
    let data = raw.split("/");
    let time = parseInt(data.shift() || '0');

    return {
        time,
        state: data.map(rawGameStateData)
    }

}

export function rawDataToCSV (raw: string): string {
    let data = raw.split("/");
    let time = data.shift() || '0';
    return data.map(e => `${time};${e}`).join("\n");
}

export function parseStdErr(raw: string): string[] {
    let data: string[] = raw.split('/');

    switch(data[0]){
        case "ERR":
            
            break;
        case "LOG":

            break;
    }

    return [""];
}

/*
let mockupData = "0/0;123.4;321.4;2.1;1;;;;;;;/1;502;205;;;12;2;2;1;0;;/2;;;;;;;;;;0;2"
let csv = rawDataToCSV(mockupData);
let game = parseGameState(mockupData);

console.log("=== CSV ===");
console.log(csv);

console.log("\n=== Game Data ===");
console.log(JSON.stringify(game, undefined, 4));
*/

/* Résultat:

=== CSV ===
0;0;123.4;321.4;2.1;1;;;;;;;
0;1;502;205;;;12;2;2;1;0;;
0;2;;;;;;;;;;0;2

=== Game Data ===
{
    "time": 0,
    "state": [
        {
            "type": "robot",
            "x": 123.4,
            "y": 321.4,
            "alpha": 2.1,
            "robot_id": 1
        },
        {
            "type": "gato",
            "x": 502,
            "y": 205,
            "pile_id": 12,
            "pile_height": 2,
            "slices": [
                "jaune",
                "marron",
                "invalid"
            ]
        },
        {
            "type": "cerise",
            "cherry_count": 2,
            "cherry_zone": 0
        }
    ]
}

*/
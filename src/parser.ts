export const enum CommandType {
    FOCUS="focus",
    CAMERA="camera",
    TEAM="team",
    STREAM="stream",
    RESULT="result"
}

export const enum CommandOption {
    FOCUS_AUTOFOCUS="auto",
    FOCUS_RESET="reset",
    FOCUS_GET="get",
    FOCUS_SET="set",
    
    CAMERA_GET="get",
    CAMERA_SET="set",
    CAMERA_OVERLAY="overlay",
    CAMERA_RAW="raw",
    CAMERA_LIST="list",

    TAG_SET="settag",
    TAG_GET="gettag",
    TEAM_SET="setteam",
    TEAM_GET="getteam",
    TEAM_SAVE="save",
    TEAM_BATCH="batch",

    STREAM_START="start",
    STREAM_KILL="stop",
    STREAM_GET="get",
    STREAM_TAG="data",
    STREAM_PERF="stats",
    STREAM_SYNC="sync"
}

export type command = {
    command: string,
    option: string,
    argument: string[]
}

export function parseCmd(command: string, input: string[]): command {
    if(!command) return null;
    if(!input) input = [];

    let split: string[] = command.split('_');

    let cmd: command = {
        command: split[0],
        option: split[1],
        argument: input
    };

    if(!cmd.command || !cmd.option) return null;
    
    return cmd;
}

export class Response {
    data: string[];

    constructor(code: number, data?: string[]){
        this.data = [code+""];
        if(data) this.data = this.data.concat(data);
    }

    static OK = 0;
    static ERROR = 1;
    static BUSY = 2;
    static BAD_COMMAND = 3;
    static MISSING_ARGUMENTS = 4;
}

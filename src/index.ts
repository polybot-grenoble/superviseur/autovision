import { AutoVision } from './autovision.js';
import { Logger, LogLevel, LogOrigin } from './logger.js';
import { AbstractNetwork, MQTTCliNet, TCPCliNet, TCPServerNet, generateResultDataFrame, netData } from './network.js';
import dotenv from 'dotenv'
import { exit } from 'process';
import { command, CommandOption, CommandType, parseCmd } from './parser.js';
import { GameState } from './ipc.js';

dotenv.config()

const logger = new Logger(process.env.DEBUG == "true");
const vision = new AutoVision(logger);

let connector: AbstractNetwork;

const HOST = process.env.HOST || "0.0.0.0";
const PORT = parseInt(process.env.PORT) || 3000;

switch(process.env.MODE){
    case 'tcpsrv':
        connector = new TCPServerNet(HOST,PORT);
        break;
    case 'mqtt':
        connector = new MQTTCliNet(HOST,PORT);
        break;
    case 'tcpcli':
        connector = new TCPCliNet(HOST,PORT);
        break;
    default:
        logger.log("Network interface not implemented.", LogLevel.CRITICAL, LogOrigin.NET);
        exit(1)
}

logger.log("Network interface: " + process.env.MODE, LogLevel.INFO, LogOrigin.NET);


connector.onReceive(processNetData);

connector.listen(function(){
    logger.log("Listening on " + HOST + ":" + PORT, LogLevel.INFO, LogOrigin.NET);
});

broadcastTags();



function processNetData(frame: netData, err: string){    
    if(err) return logger.log(err, LogLevel.ERROR, LogOrigin.NET);
    if(!frame || frame.sender==process.env.NET_NAME) return;

    logger.log("RECEIVED " + frame.command + " WITH " + (frame.data.length == 0 ?  "EMPTY" : frame.data) + " FROM " + frame.sender , LogLevel.DEBUG, LogOrigin.NET);

    //pass command to handlers
    vision.execCommand(parseCmd(frame.command, frame.data), function(res: any) {
        
        //generate result
        if(frame.command == CommandType.STREAM  + "_" + CommandOption.STREAM_GET){
            frame.data = [];
            frame.gameData = <GameState>JSON.parse(res.data[1]);
        }else{
            frame.data = res.data;
        }
        frame = generateResultDataFrame(frame);     
        
        logger.log("SENT " + frame.command + " WITH " + (frame.data.length == 0 ? "EMPTY" : frame.data) + " TO " + frame.receiver , LogLevel.DEBUG, LogOrigin.NET);
        
        //sending result
        connector.emit(frame, function(err){
            if(err) logger.log("Failed to emit dataFrame: " + err, LogLevel.ERROR, LogOrigin.NET);
        });

    });
}


function broadcastTags(): void{
    const interval: number = parseInt(process.env.MQTT_LOOP_MS) || 200;
    const cmd: command = parseCmd(CommandType.STREAM  + "_" + CommandOption.STREAM_GET, []);
    let result: netData = {command: "data", sender: process.env.NET_NAME};
        
    setInterval(function(){
        if(!AutoVision.stream_busy) return;

        vision.execCommand(cmd, function(res: any){
            if (res.data[0] == "1") return;
            result.gameData = <GameState>JSON.parse(res.data[1]);

            logger.log("SENT " + result.command + " WITH " + result.data + " TO EVERYONE", LogLevel.DEBUG, LogOrigin.NET);
            
            connector.broadcast(result, function(err){
                if(err) logger.log("Failed to broadcast dataFrame: " + err, LogLevel.ERROR, LogOrigin.NET);
            });
        });
    },interval);

    logger.log('Sending tags every ' + interval + 'ms when stream is started.', LogLevel.INFO, LogOrigin.NET);
}
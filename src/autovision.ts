import { existsSync, mkdirSync, readFileSync } from "node:fs";
import { CommandType, command, Response } from "./parser.js"
import { FocusHandler } from './handler/focusHandler.js'
import { camera, CameraHandler } from './handler/cameraHandler.js'
import { StreamHandler } from "./handler/streamHandler.js";
import { Logger, LogLevel, LogOrigin } from "./logger.js";
import { team, TeamHandler } from "./handler/teamHandler.js";
import { exit } from "node:process";
import { execFileSync } from "node:child_process";

export class AutoVision {
    config                  : config;
    private logger          : Logger;

    private focusHandler    : FocusHandler;
    private cameraHandler   : CameraHandler;
    private teamHandler     : TeamHandler;

    streamHandler           : StreamHandler;

    static camera_busy      : boolean;
    static stream_busy      : boolean;
    static config_busy      : boolean;
    static team_busy        : boolean;

    constructor(logger: Logger){
        this.logger = logger;

        AutoVision.camera_busy = false;
        AutoVision.stream_busy = false;
        AutoVision.config_busy = false;
        AutoVision.team_busy   = false;

        this.generateConfig();

        this.cameraHandler = new CameraHandler(logger, this.config.cameras, process.env.FORCE_CAMERA);
        this.focusHandler = new FocusHandler(logger, this.cameraHandler.camera.type);
        this.teamHandler = new TeamHandler(logger, this.config.team);
        this.streamHandler = new StreamHandler(logger);

        this.logger.log("Ready to accept requests.", LogLevel.INFO, LogOrigin.MAIN);
    }

    execCommand(cmd: command, cb: any): void {
        if(!cmd) return cb(new Response(Response.BAD_COMMAND));
            
        switch(cmd.command){
            case CommandType.FOCUS :
                this.focusHandler.handleCommand(cmd,cb);
                break;
            case CommandType.CAMERA:
                this.cameraHandler.handleCommand(cmd,cb);
                break;
            case CommandType.TEAM:
                this.teamHandler.handleCommand(cmd,cb);
                break;
            case CommandType.STREAM:
                this.streamHandler.handleCommand(cmd,cb);
                break;
            default:
                cb(new Response(Response.BAD_COMMAND));
                break;
        }
    }

    private generateConfig(): void {
        if(AutoVision.config_busy){
            this.logger.log("Config file is busy.", LogLevel.ERROR, LogOrigin.CONFIG);
            return;
        }
        AutoVision.config_busy = true;

        try{
            if(!existsSync(process.env.VISION_CONFIG_FOLDER)) mkdirSync(process.env.VISION_CONFIG_FOLDER);
            if(!existsSync(process.env.VISION_IMAGE_FOLDER)) mkdirSync(process.env.VISION_IMAGE_FOLDER);
        }catch(err){
            this.logger.log("Failed to create folders.", LogLevel.CRITICAL);
            exit(1);
        }

        try{
            this.config = <config>JSON.parse(readFileSync("config/config.json", 'utf8'));
        }catch(err){
            this.logger.log("Failed to read config file.", LogLevel.CRITICAL);
            exit(1);
        }
    
        if(!this.config){
            this.logger.log("Config file is empty.", LogLevel.CRITICAL);
            exit(1);
        }

        if(this.config.cameras.length == 0){
            this.logger.log("No cameras are configured.", LogLevel.CRITICAL);
            exit(1);
        }

        try{
            for(let i = 0; i<this.config.cameras.length; i++){
                if(!existsSync(process.env.VISION_CONFIG_FOLDER+this.config.cameras[i].name+"/")) mkdirSync(process.env.VISION_CONFIG_FOLDER+this.config.cameras[i].name+"/");
            }
        }catch(err){
            this.logger.log("Failed to create camera folders.", LogLevel.CRITICAL);
            exit(1);
        }
    
        if(!existsSync(process.env.VISION_CONFIG_FOLDER+process.env.VISION_CONFIG_FILE)){
            this.logger.log("Vision config file not found. Generating it.", LogLevel.WARNING);
            
            try{
                execFileSync(process.env.VISION_EXE_FOLDER + process.env.INIT_CONFIG_EXE,[process.env.VISION_CONFIG_FOLDER+process.env.VISION_CONFIG_FILE],{timeout: 5000});
                this.logger.log("Vision config file created.");
            }catch(err){
                this.logger.log("Failed to create config file. Code: " + err.code, LogLevel.CRITICAL);
                exit(1);
            }
        }

        AutoVision.config_busy = false;
    }
}



export type config = {
    team: team;
    cameras: camera[];
}

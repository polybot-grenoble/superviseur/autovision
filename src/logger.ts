import { createWriteStream, existsSync, mkdirSync, WriteStream } from "fs";

export class Logger {

    private stream: WriteStream;
    private debug: boolean;

    constructor(debug: boolean){
        if(!existsSync("logs")) mkdirSync("logs");
        this.debug = debug;
        this.stream = createWriteStream(fileNameFormat(),{encoding: 'utf-8', flags: 'a'});
    }

    log(message: string, level?: LogLevel, origin?: LogOrigin){
        if(!level) level=LogLevel.INFO;
        if(!this.debug && level==LogLevel.DEBUG) return;
        if(!origin) origin=LogOrigin.MAIN;
        let log = dateTimeFormat() + " " + origin.toString() + " " + level.toString() + " " + message;
        console.log(log);
        this.stream.write(log+'\n',function(){})
    }
}

function dateTimeFormat(): string {
    return new Date(Date.now()).toISOString().replace("T", " ").replace(".", ",").replace("Z", "");
}

function fileNameFormat(): string {
    let d = new Date(Date.now()).toISOString();
    d=d.replace(/[-:T]/g, "_").replace(/[Z.]/g,"");
    return "logs/log_SUPERVISEUR_"+d;
}

export const enum LogLevel {
    DEBUG   ="[DEBUG]",
    INFO    ="[INFO ]",
    WARNING ="[WARNI]",
    ERROR   ="[ERROR]",
    CRITICAL="[CRITI]"
}

export const enum LogOrigin {
    MAIN    = "[Main   ]",
    NET     = "[Network]",
    TEAM    = "[Team   ]",
    CONFIG  = "[Config ]",
    STREAM  = "[Stream ]"
}
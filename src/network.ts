import { Socket, createServer, Server } from "net";
import { connect, MqttClient } from "mqtt";
import { GameState } from "./ipc.js";

/* Abstract network class. Can be either used as a client or a server. */
export abstract class AbstractNetwork {

    protected host: string;
    protected port: number;

    constructor(host?: string, port?: number) {
        this.host = host;
        this.port = port;
    }

    /* Listen for clients on the specified port */
    abstract listen(cb: callbackListen): void;

    /* Emit netData packet. The callback is processed once the packet is sent. */
    abstract emit(data: netData, cb: callbackEmit): void;

    /* Broadcast netData packet. */
    abstract broadcast(data: netData, cb: callbackEmit): void;

    /* Callback when a netData packet is received. */
    abstract onReceive(cb: callbackReceive): void;

    /* Parse a netData packet from a string. */
    protected parseData(data: string): netData {
        let res: netData;

        try {
            res = <netData>JSON.parse(data.toString());
        } catch (err) {
            throw new Error("Invalid dataframe.");
        }

        if (!res.sender) throw new Error("No sender specified.");
        if (!res.receiver) res.receiver = process.env.NET_NAME;

        return res;
    }
}

/* TCP server network class */
export class TCPServerNet extends AbstractNetwork {
    private server: Server;
    private clients: Map<string, Socket>;

    constructor(host?: string, port?: number) {
        super(host, port);
        this.clients = new Map();
    }

    listen(cb: callbackListen): void {
        this.server.listen(this.port, this.host, cb);
    }

    emit(data: netData, cb: callbackEmit): void {
        //Find the receiver's socket.
        let cli: Socket = this.clients.get(data.receiver);
        if (!cli) return cb("Client does not exist.");

        //Write to the socket.
        cli.write(JSON.stringify(data) + '\n', (err) => {
            if (err) return cb(err.message);
            cb(null);
        });
    }

    broadcast(data: netData, cb: callbackEmit): void {
        console.log(this.clients)
        this.clients.forEach((cli,name)=>{
            data.receiver = name;
            cli.write(JSON.stringify(data) + '\n', (err) => {
                if (err) return cb(err.message);
                cb(null);
            });
        });
    }

    onReceive(cb: callbackReceive): void {
        this.server = createServer((sock: Socket) => {
            //Code used by each socket.
            sock.on('data', (data) => {
                try {
                    let temp: netData = this.parseData(data.toString());
                    if (!this.clients.has(temp.sender)) this.clients.set(temp.sender, sock); //Map client
                    cb(temp, null);
                } catch (err) {
                    cb(null, err);
                }
            });

            //Error handler
            sock.on('error', (err) => {
                cb(null, err.message);
            })

            //Close handler
            sock.on('close', () => {
                //Find socket and remove it from the map.
                this.clients.forEach((value, key) => {
                    if (sock.remoteAddress == value.remoteAddress) this.clients.delete(key);
                });
            })
        });
    }
}

/* MQTT client network class */
export class MQTTCliNet extends AbstractNetwork {

    private client: MqttClient;

    constructor(host?: string, port?: number) {
        super(host, port);

        this.client = connect({
            servers: [{ host: this.host, port: this.port }],
            clientId: process.env.NET_NAME,
            keepalive: 4,

            //Timeout after 3 seconds. Be quick, a game is only 100 seconds.
            connectTimeout: 3 * 1000,

            //Client behaviour on disconnect.
            will: {
                topic: TOPIC.ROOT + TOPIC.STATUS,
                payload: JSON.stringify({ sender: process.env.NET_NAME, data: [process.env.MQTT_WILL_MESSAGE] }),
                qos: 0,
                retain: true
            }
        });
    }

    listen(cb: callbackListen): void {
        //Listen for messages on command topic
        this.client.subscribe(TOPIC.ROOT + TOPIC.COMMAND, null, (err, granted) => {
            cb();
        });
    }

    emit(data: netData, cb: callbackEmit): void {
        //Send to command topic, or data if it's game related data
        let topic: string = TOPIC.ROOT + (data.command == TOPIC.DATA ? TOPIC.DATA : TOPIC.COMMAND);

        this.client.publish(topic, JSON.stringify(data), (err) => {
            if (err) return cb(err.message);
            cb(null);
        });
    }

    broadcast(data: netData, cb: callbackEmit): void {
        //Use emit because everyone who subscribes to a topic can receive its messages.
        this.emit(data,cb);
    }

    onReceive(cb: callbackReceive): void {

        //Message handler
        this.client.on('message', (topic, message) => {

            //Filter topics
            if (topic == (TOPIC.ROOT + TOPIC.COMMAND)) {
                try {
                    cb(this.parseData(message.toString()), null);
                } catch (err) {
                    cb(null, err);
                }
            }
        });

        //Error handler
        this.client.on('error', (err) => {
            cb(null, err.message);
        })

        //Connect handler
        this.client.on('connect', () => {
            this.client.publish(TOPIC.ROOT + TOPIC.STATUS, JSON.stringify({ sender: process.env.NET_NAME, data: [process.env.MQTT_CONNECT_MESSAGE] }), { retain: true });
        })
    }
}

/* TCP client network class */
export class TCPCliNet extends AbstractNetwork {

    private socket: Socket;

    constructor(host?: string, port?: number) {
        super(host, port);
        this.socket = new Socket();
    }

    listen(cb: callbackListen): void {
        this.socket.connect({ port: this.port, host: this.host, keepAlive: true }, cb);
    }

    emit(data: netData, cb: callbackEmit): void {
        //Write netData to server
        this.socket.write(JSON.stringify(data), (err) => {
            if (err) return cb(err.message);
            cb(null);
        });
    }

    broadcast(data: netData, cb: callbackEmit): void {
        //Depends of the server.
        this.emit(data, cb);
    }

    onReceive(cb: callbackReceive): void {
        //On message handler
        this.socket.on('data', (data) => {
            try {
                cb(this.parseData(data.toString()), null);
            } catch (err) {
                cb(null, err);
            }
        });

        //Error handler
        this.socket.on('error', (err) => {
            cb(null, err.message);
        });
    }

}

/* Typescript types to specify the callback used by the classes. */
export type callbackListen = () => void;
export type callbackEmit = (err: string) => void;
export type callbackReceive = (data: netData, err: string) => void;

/* Prepares netdata result */
export function generateResultDataFrame(data: netData): netData {
    let t: string = data.receiver;
    data.receiver = data.sender;
    data.sender = t;
    return data;
}

/* Topics used by MQTT. ROOT+STATUS, ROOT+COMMAND, ROOT+DATA */
const enum TOPIC {
    ROOT = "root/superviseur/",
    STATUS = "status",
    COMMAND = "command",
    DATA = "data",
}

/* netData packet */
export type netData = {
    sender?: string,        //IPv4 or predefined name (ex: michel)
    receiver?: string,      //IPv4 or predefined name (ex: superviseur)
    command?: string,       //A command. "result" when the frame is the result of a command.
    data?: string[]         //Can be empty
    gameData?: GameState;   //Can be empty. Specific to this year rules.
}
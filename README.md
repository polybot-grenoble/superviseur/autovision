# Autovision
(Not so small) NodeJS Typescript server to control [vision](https://gitlab.com/polybot-grenoble/superviseur/vision)'s process and everything around it.

## Install
Steps for Ubuntu install
```\bash
sudo apt install nodejs
npm i -g n
sudo n lts //Update NodeJS to LTS

//Update

git clone https://gitlab.com/polybot-grenoble/superviseur/autovision.git
cd autovision

npm i // Install dependencies
npm run build // Build the project

node dist/index.js // Run autovision
```

## Wiki
For detailed explanations about this system, check the [wiki](https://gitlab.com/polybot-grenoble/superviseur/autovision/-/wikis/home) page.